import mongoose = require('mongoose');
// import * as mongoose from 'mongoose';

const uri: string = 'mongodb://127.0.0.1:27017/local';

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }, (err: any) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log('Successfully Connected!');
  }
});

export interface IHero extends mongoose.Document {
  name: string;
}

export const HeroSchema = new mongoose.Schema({
  name: { type: String, required: true }
});

const Hero = mongoose.model<IHero>('Hero', HeroSchema);

export default Hero;