import { Request, Response } from 'express';
import Hero from './../hero';

export const allHeroes = (req: Request, res: Response) => {
  Hero.find((err: any, heroes: any) => {
    if (err) {
      res.send('Error!');
    } else {
      res.send(heroes);
    }
  });
};

export const getHero = (req: Request, res: Response) => {
  Hero.findById(req.params.id, (err: any, hero: any) => {
    if (err) {
      res.send(err);
    } else {
      res.send(hero);
    }
  });
};

export const addHero = (req: Request, res: Response) => {

  const hero = new Hero(req.body);

  hero.save((err: any) => {
    console.log(err);
    if (err) {
      res.send(err);
    } else {
      res.send(hero);
    }
  });
};

export const deleteHero = (req: Request, res: Response) => {
  console.log(req.params);

  Hero.findOneAndDelete({ _id: req.params.id }, (err: any, doc) => {
    if (err) {
      res.send(err);
    } else {
      res.json('test');
    }
  });
};

export const updateHero = (req: Request, res: Response) => {
  Hero.findByIdAndUpdate(
    req.params.id,
    req.body,
    (err: any) => {
      if (err) {
        res.send(err);
      } else {
        res.json('test');
      }
    }
  );
};

export const searchHero = (req: Request, res: Response) => {
  const { name } = req.params;
  Hero.find({ name: { $regex: `.*${name}.*` } }, (err: any, hero: any) => {
    if (err) {
      res.send(err);
    } else {
      res.json(hero);
    }
  });
};