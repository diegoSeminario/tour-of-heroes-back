import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import * as heroController from './controllers/heroController';

// Our Express APP config
const app = express();

app.use(cors());

app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());

// API Endpoints
app.get('/', (req: Request, res: Response) => res.send('hi'));

// API Endpoints
app.get('/heroes', heroController.allHeroes);
app.get('/heroes/:id', heroController.getHero);
app.post('/heroes', heroController.addHero);
app.put('/heroes/:id', heroController.updateHero);
app.delete('/heroes/:id', heroController.deleteHero);
app.get('/heroes/:name/search', heroController.searchHero);

const server = app.listen(app.get('port'), () => {
  console.log('App is running on http://localhost:%d', app.get('port'));
});