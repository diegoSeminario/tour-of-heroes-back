"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HeroSchema = void 0;
var mongoose = require("mongoose");
// import * as mongoose from 'mongoose';
var uri = 'mongodb://127.0.0.1:27017/local';
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
    if (err) {
        console.log(err.message);
    }
    else {
        console.log('Successfully Connected!');
    }
});
exports.HeroSchema = new mongoose.Schema({
    name: { type: String, required: true }
});
var Hero = mongoose.model('Hero', exports.HeroSchema);
exports.default = Hero;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVyby5qcyIsInNvdXJjZVJvb3QiOiIuL3NyYy8iLCJzb3VyY2VzIjpbImhlcm8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsbUNBQXNDO0FBQ3RDLHdDQUF3QztBQUV4QyxJQUFNLEdBQUcsR0FBVyxpQ0FBaUMsQ0FBQztBQUV0RCxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLGVBQWUsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLEVBQUUsVUFBQyxHQUFRO0lBQ2xGLElBQUksR0FBRyxFQUFFO1FBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDMUI7U0FBTTtRQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztLQUN4QztBQUNILENBQUMsQ0FBQyxDQUFDO0FBTVUsUUFBQSxVQUFVLEdBQUcsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDO0lBQzVDLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtDQUN2QyxDQUFDLENBQUM7QUFFSCxJQUFNLElBQUksR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFRLE1BQU0sRUFBRSxrQkFBVSxDQUFDLENBQUM7QUFFdkQsa0JBQWUsSUFBSSxDQUFDIn0=